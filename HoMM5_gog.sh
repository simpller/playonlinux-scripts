#!/bin/bash
# Date : (2015-10-11)
# Last revision : (2015-10-11)
# Wine version used : 1.7.52
# Distribution used to test : Linux Mint 17.2
# Author : Jylhis
# Script licence : MIT
# Program licence : Retail


# TODO
# - The install icon is 22x22
# - The game icon is 32x32 or more
# - The miniature is 200x150

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

TITLE="GOG.com - Heroes of Might and Magic 5: Bundle"
PREFIX="HoMM5_gog"
GOGID="heroes_of_might_and_magic_5_bundle"
WORKING_WINE_VERSION="1.7.52"
GAME_VMS="256"

# Start the script
POL_SetupWindow_Init

# Start debug API
POL_Debug_Init

POL_SetupWindow_presentation "$TITLE" "Nival Interactive / Ubisoft" "http://www.gog.com/game/$GOGID" "Jylhis" "$PREFIX"

# Asks wich one to install
POL_SetupWindow_menu_num "Which one you want to install?" "$TITLE" "HoMM5|TOTE(not working)" "|"

# Set shortcut name
if [ "$APP_ANSWER" = 1 ]
then
	SHORTCUT_NAME="Heroes of Might and Magic 5: Tribes Of The East"
elif [ "$APP_ANSWER" = 0 ]
then
	SHORTCUT_NAME="Heroes of Might and Magic 5"
fi

# Setting prefix path
POL_Wine_SelectPrefix "$PREFIX"

# Downloading wine if necessary and creating prefix
POL_System_SetArch "auto"
POL_Wine_PrefixCreate "$WORKING_WINE_VERSION" # Is this nessecary? TODO

# GoG installers gives few errors at the end
POL_SetupWindow_message "$(eval_gettext 'At the end of the install progres there will appear few error messages; just ignore them!')"

# TODO: Check md5sum
POL_Call POL_GoG_setup "$GOGID" "74f32ce4fd9580842d6f4230034c04ba" "80db7a846cb8a7052506db814cb2185e"
POL_Call POL_GoG_install

# Set OS to win7. Is this nessecary? TODO
Set_OS win7

# Asking about memory size of graphic card
POL_SetupWindow_VMS $GAME_VMS

POL_Wine_reboot # yes/no? TODO

# Making shortcuts
POL_Shortcut "H5_Game.exe" "$SHORTCUT_NAME" "" "" "Game;Strategy;"
POL_Shortcut "H5_MapEditor.exe" "$SHORTCUT_NAME - MapEditor" "" "" "Game;Strategy;"

if ["$APP_ANSWER" = "1"]
then
	POL_Shortcut "skillwheel.exe" "$SHORTCUT_NAME - Skillwheel" "" "" "Game;Strategy;"
fi

# Documentation. TODO fanDoc V3.0
if ["$APP_ANSWER" = "0"]
then
	POL_Shortcut_Document "$SHORTCUT_NAME" "$GOGROOT/Heroes of Might and Magic V/Game Manual.pdf"
elif ["$APP_ANSWER" = "1"]
then
	POL_Shortcut_Document "$SHORTCUT_NAME" "$GOGROOT/Heroes of Might and Magic V/Game Manual 2.0.pdf"
fi

# Exit
POL_SetupWindow_Close
exit 0
