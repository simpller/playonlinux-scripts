#!/bin/bash
# Date : (2015-10-15)
# Last revision : (2015-10-15)
# Wine version used : 1.7.52
# Distribution used to test : Linux Mint 17.2
# Author : Jylhis
# Script licence : MIT
# Program licence : Retail

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

TITLE="The Simpsons: Hit & Run"
PREFIX="Simpsons_HaT"
WORKING_WINE_VERSION="1.7.52"

# Start the script
POL_SetupWindow_Init

# Start debug API
POL_Debug_Init

# Presentation
POL_SetupWindow_presentation "$TITLE" "Radical Entertainment" "" "Jylhis" "$PREFIX"

# Setting prefix path
POL_Wine_SelectPrefix "$PREFIX"

# Downloading wine if necessary and creating prefix
POL_System_SetArch "X86"
POL_Wine_PrefixCreate "$WORKING_WINE_VERSION" # Is this nessecary? TODO

# TODO: Ask dvd or exe. Install
POL_SetupWindow_InstallMethod "DVD"

if [ "$INSTALL_METHOD" = "DVD" ]; then
  POL_SetupWindow_cdrom
  POL_SetupWindow_check_cdrom "Simpsons.ICO"
  cd "$WINEPREFIX/dosdevices"
  ln -sf "$CDROM" p:
  POL_Wine start /unix "$CDROM/Setup.exe"
  POL_Wine_WaitExit "$TITLE"
fi

# Set OS to win7. Is this nessecary? TODO
Set_OS winxp

# Asking about memory size of graphic card
POL_SetupWindow_VMS $GAME_VMS

POL_Wine_reboot # yes/no? TODO

# Making shortcuts
POL_Shortcut "Simpsons.exe" "$TITLE" "" "" "Game;"

# Exit
POL_SetupWindow_Close
exit 0
